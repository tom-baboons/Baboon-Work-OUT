import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';
import WelcomeScreen from './app/screens/WelcomeScreen';
import Card from './app/components/Card'
import AppButton from './app/components/AppButton';

import colors from './app/config/colors';
import ListingDetailsScreen from './app/screens/ListingDetailsScreen';

export default function App() {
  return (
    
      <View style={styles.innersection}>
        <ListingDetailsScreen
          title="Push Workout"
          subTitle="Was zwaar baas"
          time="Yesterday" />
      </View>

  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.dark, 
    flex: 1,
  },
  innersection: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    padding: 20,
    backgroundColor: colors.dark
  }
});
