import React from 'react'
import { ImageBackground, Image, StyleSheet, Text, View } from 'react-native'
import AppButton from '../components/AppButton'

import colors from '../config/colors'

export default function WelcomeScreen() {
    return (
        
        <View style={styles.background}>
            <View style={styles.logoContainer}>
                <Image style={styles.logo} source={require("../assets/Logo_BaboonOut.png")}/>
            </View>
            <View style={styles.buttonsContainer}>

            </View>
            <AppButton title="Jeh" onPress={() => console.log("Hatseflats")}></AppButton>
            <AppButton title="Neh" color="secondary" onPress={() => console.log("Jeh Werkt moatje")}></AppButton>
        </View>
    )
}

const styles = StyleSheet.create({
    background: {
        backgroundColor: colors.dark, 
        flex: 1,
        justifyContent: "flex-end",
        alignItems: 'center'
    },
    logoContainer: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: 'center',
        top: 170,
    },
    logo : {
        width: 220,
        height: 220,
        resizeMode: "contain",
    },
    buttonsContainer : {

    }
})
