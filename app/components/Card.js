import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import colors from '../config/colors'

export default function Card({title, subTitle, time}) {
    return (
        <View style={styles.col}>
            <View style={styles.card}>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.subTitle}>{subTitle}</Text>
                <Text style={styles.time}>{time}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    col : {
        padding: 10,
        width: "50%",
        minHeight: 200,
    },
    card: {
        borderRadius: 12,
        backgroundColor: colors.lightDark,
        padding: 20,
        
        flex: 1,  
    },
    title: {
        fontSize: 18,
        color: colors.white,
        fontWeight: "800",
        marginBottom: 10,
    },
    subTitle: {
        fontSize: 12,
        lineHeight: 18,
        color: colors.lightText,
        marginBottom: 10,
    },
    time: {
        fontSize: 10,
        color: colors.primary,
    }
})
