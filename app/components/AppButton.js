import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'

import colors from '../config/colors'

export default function AppButton({title, onPress, color = "primary" }) {
    return (
        <TouchableOpacity style={[styles.button, { backgroundColor: colors[color] }]} onPress={onPress}>
            <Text style={styles.text}>{title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        width: "40%",
        height: 50,
        backgroundColor: colors.primary,
        bottom: 20,
        borderRadius: 100,
        justifyContent: "center",
        alignItems: 'center',
        marginVertical: 10
    },
    text: {
        color: colors.white,
        fontSize: 16,
        fontWeight: "600",
    }

})
